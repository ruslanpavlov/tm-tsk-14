package ru.tsc.pavlov.tm.service;

import ru.tsc.pavlov.tm.api.repository.ICommandRepository;
import ru.tsc.pavlov.tm.api.service.ICommandService;
import ru.tsc.pavlov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
