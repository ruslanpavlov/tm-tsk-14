package ru.tsc.pavlov.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void info();

    void exit();

    void ShowErrorCommand();

    void ShowErrorArgument();

}
