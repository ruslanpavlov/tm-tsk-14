package ru.tsc.pavlov.tm.api.entity;

import java.util.Date;

public interface IHasCreated {

    void setCreated(Date created);

    Date getCreated();
}
